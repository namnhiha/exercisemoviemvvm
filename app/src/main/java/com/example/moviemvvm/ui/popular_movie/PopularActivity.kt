package com.example.moviemvvm.ui.popular_movie

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moviemvvm.R
import com.example.moviemvvm.data.api.TheMovieDBClient
import com.example.moviemvvm.data.repository.NetworkState
import kotlinx.android.synthetic.main.activity_main.*

class PopularActivity : AppCompatActivity() {
    private lateinit var viewModel: PopularViewModel
    lateinit var movieRepository: MoviePagedListRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val apiService = TheMovieDBClient.getClient()
        movieRepository = MoviePagedListRepository(apiService)
        viewModel = getViewModel()
        val movieAdapter = PopularMoviePagedListAdapter(this)
        val gridLayoutManager = GridLayoutManager(this, 4)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val viewType = movieAdapter.getItemViewType(position)
                if (viewType == movieAdapter.MOVIE_VIEW_TYPE) return 1
                else return 3
            }
        }
        recyclerViewList.layoutManager = gridLayoutManager
        recyclerViewList.setHasFixedSize(true)
        recyclerViewList.adapter = movieAdapter
        viewModel.moviePagedList.observe(this, Observer {
            movieAdapter.submitList(it)
        })
        viewModel.networkState.observe(this, Observer {
            progressBarPopular.visibility =
                if (viewModel.listIsEmpty() && it == NetworkState.LOADING) View.VISIBLE else View.GONE
            textViewError.visibility =
                if (viewModel.listIsEmpty() && it == NetworkState.ERROR) View.VISIBLE else View.GONE
            if (!viewModel.listIsEmpty()) {
                movieAdapter.setNetWorkState(it)
            }
        })
    }

    private fun getViewModel(): PopularViewModel {
        val factory = PopularViewModelFactory(movieRepository)
        return ViewModelProvider(this, factory).get(PopularViewModel::class.java)
    }
}
