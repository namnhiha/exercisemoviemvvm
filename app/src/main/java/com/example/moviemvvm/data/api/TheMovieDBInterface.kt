package com.example.moviemvvm.data.api

import com.example.moviemvvm.data.model.MovieDetails
import com.example.moviemvvm.data.model.MovieResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheMovieDBInterface {
    //
    //https://api.themoviedb.org/3/movie/299534?api_key=1cf151ff23247b3fe3558107667a395f
    @GET("movie/{movie_id}")
    fun getMovieDetail(@Path("movie_id") id: Int): Single<MovieDetails>

    //
    //https://api.themoviedb.org/3/movie/popular?api_key=1cf151ff23247b3fe3558107667a395f&language=en-US&page=1
    @GET("movie/popular")
    fun getPopularMovie(@Query("page") page: Int): Single<MovieResponse>
}
