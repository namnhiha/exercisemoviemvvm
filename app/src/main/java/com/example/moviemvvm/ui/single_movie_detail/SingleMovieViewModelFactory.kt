package com.example.moviemvvm.ui.single_movie_detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SingleMovieViewModelFactory(
    private var movieRepository: MovieDetailRepository,
    val movieId: Int
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(MovieDetailRepository::class.java, Int::class.java)
            .newInstance(movieRepository, movieId)
    }
}
