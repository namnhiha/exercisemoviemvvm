package com.example.moviemvvm.ui.single_movie_detail

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.moviemvvm.R
import com.example.moviemvvm.data.api.POSTER_BASE_URL
import com.example.moviemvvm.data.api.TheMovieDBClient
import com.example.moviemvvm.data.api.TheMovieDBInterface
import com.example.moviemvvm.data.repository.NetworkState
import com.example.moviemvvm.data.model.MovieDetails
import kotlinx.android.synthetic.main.activity_single_movie.*
import java.text.NumberFormat
import java.util.*

class SingleMovieActivity : AppCompatActivity() {
    private lateinit var viewModel: SingleMovieViewModel
    private lateinit var movieRepository: MovieDetailRepository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_movie)
        val movieId = intent.getIntExtra("id", 1)
        val apiService: TheMovieDBInterface = TheMovieDBClient.getClient()
        movieRepository = MovieDetailRepository(apiService)
        viewModel = getViewModel(movieId, movieRepository)
        viewModel.movieDetails.observe(this, Observer {
            bindUI(it)
        })
        viewModel.networkState.observe(this, Observer {
            progressBar.visibility = if (it == NetworkState.LOADING) View.VISIBLE else View.GONE
            textViewError.visibility = if (it == NetworkState.ERROR) View.VISIBLE else View.GONE
        })
    }

    fun bindUI(it: MovieDetails) {
        textViewTitle.text = it.title
        textViewSubTitle.text = it.tagline
        textViewReleaseDate.text = it.releaseDate
        textViewMovieRating.text = it.rating.toString()
        textViewMovieRuntime.text = it.runtime.toString()
        textViewOverview2.text = it.overview
        val formatCurrency = NumberFormat.getCurrencyInstance(Locale.US)
        textViewMovieBucket.text = formatCurrency.format(it.budget)
        textViewMovieRevenue.text = formatCurrency.format(it.revenue)
        val moviePosterUrl = POSTER_BASE_URL + it.posterPath
        Glide.with(this)
            .load(moviePosterUrl)
            .into(imageViewMovie)
    }

    private fun getViewModel(
        movieId: Int,
        repository: MovieDetailRepository
    ): SingleMovieViewModel {
        val factory = SingleMovieViewModelFactory(movieRepository, movieId)
        return ViewModelProvider(this, factory).get(SingleMovieViewModel::class.java)
    }
}
