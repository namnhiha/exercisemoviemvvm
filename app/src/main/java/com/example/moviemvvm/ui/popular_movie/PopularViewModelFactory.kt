package com.example.moviemvvm.ui.popular_movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PopularViewModelFactory(
    private var movieRepository: MoviePagedListRepository,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(MoviePagedListRepository::class.java)
            .newInstance(movieRepository)
    }
}
